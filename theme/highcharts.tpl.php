<?php

/**
 * @file
 * Default views template for displaying a slideshow.
 *
 * - $view: The View object.
 * - $options: Settings for the active style.
 * - $rows: The rows output from the View.
 * - $title: The title of this group of rows. May be empty.
 *
 * @ingroup views_templates
 */
?>
<script type="text/javascript">
<?php print $highcharts_graph_output ?>
</script>
<div id="chart-container-1" class="container-1" style="width: 100%; height: 100%"></div>