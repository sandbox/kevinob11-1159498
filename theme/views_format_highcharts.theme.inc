<?php

/**
 * @file
 * The theme system, which controls the output of highcharts.
 */

function template_preprocess_highcharts(&$vars) {
  //Make variables local.
  $options = $vars['options'];
  $view = $vars['view'];
  
  //Check the label format
  $numeric_types = array('number_integer', 'number_float');
  $date_types = array('date');
  $label_field = $options['dataset']['label'];
  //D7: Use field api function to get field info.
  $label_field_info = field_info_field($label_field);
  if (in_array($label_field_info['type'], $numeric_types)) {
    $label_format = 'numeric';
  }
  elseif (in_array($label_field_info['type'], $date_types)) {
    $label_format = 'date';
  }
  else {
    $label_format = 'string';
  }
  unset($label_field);
  unset($label_field_info);
  unset($numeric_types);
  unset($date_types);

  //Prepare data for input into chart.
  $data = array();
  $label_field_alias = 'field_' . $options['dataset']['label'];
  foreach ($options['dataset']['data'] as $field) {
    if ($field) {
      $label = $view->field[$field]->options['label'];
      $data[$label] = array();
      foreach ($view->result as $row) {
        $data_field_alias = 'field_' . $field;
        if ($label_format == 'date') {
          $data[$label][] = array('label' => (strtotime($row->{$label_field_alias}['0']['rendered']['#markup'])*1000), 'value' => $row->{$data_field_alias}['0']['rendered']['#markup']);
        }
        else {
          $data[$label][] = array('label' => $row->{$label_field_alias}['0']['rendered']['#markup'], 'value' => $row->{$data_field_alias}['0']['rendered']['#markup']);
        }
      }
    }
  }
  unset($label);
  unset($data_field_alias);

  //Set any default options not set by the user.
  $options['format']['chart']['renderTo'] = array(
    'value' => 'chart-container-1',
    'format' => 'text'
  );
  if ($label_format == 'string') {
    $options['format']['xAxis']['categories']['value'] = "";
    $options['format']['xAxis']['categories']['value'] .= "[";
    foreach ($view->result as $key => $row) {
      $label = $row->{$label_field_alias}['0']['rendered']['#markup'];
      $options['format']['xAxis']['categories']['value'] .= "'$label'";
      $options['format']['xAxis']['categories']['value'] .= $row == end($view->result) && $key == key($view->result)  ? '' : ',';
    }
    $options['format']['xAxis']['categories']['value'] .= "]";
    $options['format']['xAxis']['categories']['format'] = "list";
    unset($label);
    unset($label_field_alias);
  }
  elseif ($label_format == 'date') {
    $options['format']['xAxis']['type'] = array(
      'value' => "datetime",
      'format' => 'text'
    );
  }

  //generate js chart object
  $output = '';
  $output .= 'var chart; jQuery(document).ready(function() {chart = new Highcharts.Chart({';

  //create js for the chart options
  $output .= generate_options_js($options['format']);
  $output .= ",";

  //create js for data
  $output .= 'series: [';
  foreach ($data as $keya => $itema) {
    $output .= '{';
    $output .= "name: '" . $keya . "',";
    $output .= 'data: [';
      foreach ($itema as $keyb => $itemb) {
        $label = $itemb['label'];
        $value = $itemb['value'];
        $output .= $label_format != 'string' ? "[$label,$value]" : "$value";
        $output .= $itemb == end($itema) && $keyb == key($itema)  ? '' : ',';
      }
    $output .= ']';
    $output .= '}';
    $output .= $itema != end($data) ? ',' : '';
  }
  $output .= ']});});';

  drupal_add_js(libraries_get_path('highcharts') . '/js/highcharts.js');
  drupal_add_js(libraries_get_path('highcharts') . '/js/themes/grid.js');

  $vars['highcharts_graph_output'] = $output;
}

//iterates through the options array and prints out the heirarchy of options
function generate_options_js($options) {
  $output = '';
  foreach ($options as $key => $item) {
    if (is_array($item)) {
      if (isset($item['value'])) {
        $option_value = $item['value'];
        $output .= "$key: ";
        if ($item['format'] == 'text') {
          $output .= "'$option_value'";
        }
        elseif ($item['format'] == 'function') {
          $output .= "function() {return $option_value ;}";
        }
        elseif ($item['format'] == 'list') {
          $output .= $option_value;
        }
      }
      else {
        $output .= "$key: {";
        $output .= generate_options_js($item);
        $output .= "}";
      }
      $output .= $item !== end($options) ? ',' : '';
    }
  }
  return $output;
}