<?php
// $Id: views_plugin_style_chart.inc,v 1.1.2.17 2010/11/18 18:24:08 rsevero Exp $

/**
 * @file
 * Holds views_plugin_style_chart class which implements the chart plugin.
 */

class views_plugin_style_views_format_highchart extends views_plugin_style {
  
  function option_definition() {
    $options = parent::option_definition();
    $options['format']['contains']['title']['contains']['text']['contains'] = array(
      'value' => array('default' => 'Test Title'),
      'format' => array('default' => 'text')
    );
    $options['format']['contains']['chart']['contains']['type']['contains'] = array(
      'value' => array('default' => 'spline'),
      'format' => array('default' => 'text')
    );
    $options['format']['contains']['yAxis']['contains']['title']['contains']['text']['contains'] = array(
      'value' => array('default' => 'Test Y-axis Title'),
      'format' => array('default' => 'text')
    );
    $options['format']['contains']['tooltip']['contains']['formatter']['contains'] = array(
      'value' => array('default' => "'<b>'+this.series.name+'</b><br/>'+this.x+': '+this.y"),
      'format' => array('default' => 'function')
    );
    return $options;
  }


  /**
  * Options form.
  */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);  

    $form['format']['chart']['type']['value'] = array(
      '#title' => t('Chart Type'),
      '#type' => 'select',
      '#options' => array('spline' => 'Spline', 'line' => 'Line', 'bar' => 'Bar', 'area' => 'Area', 'areaspline' => 'Area Spline', 'column' => 'Column', 'pie' => 'Pie'),
      '#multiple' => FALSE,
      '#required' => TRUE,
      '#default_value' => $this->options['format']['chart']['type']['value'],
    );

    $form['format']['title']['text']['value'] = array(
      '#title' => t('Chart Title'),
      '#type' => 'textfield',
      '#default_value' => $this->options['format']['title']['text']['value'],
    );

    $form['format']['yAxis']['title']['text']['value'] = array(
      '#title' => t('Y-Axis Title'),
      '#type' => 'textfield',
      '#default_value' => $this->options['format']['yAxis']['title']['text']['value'],
    );

    $form['format']['tooltip']['formatter']['value'] = array(
      '#title' => t('Tool Tip Formatter'),
      '#type' => 'textarea',
      '#required' => TRUE,
      '#default_value' => $this->options['format']['tooltip']['formatter']['value'],
    );

    $highcharts_handlers = $this->display->handler->get_handlers('field');
    $highcharts_fields = array();
    foreach ($highcharts_handlers as $key => $value) {
      //Check if the field is numeric.
      //D7: Use field api function to get field info.
      $field_info = field_info_field($key);
      $numeric_types = array('number_integer', 'number_float', 'float');
      if (in_array($field_info['type'], $numeric_types) || in_array($field_info['data_type'], $numeric_types)) {
        $field_numeric = TRUE;
      }
      else {
        $field_numeric = FALSE;
      }

      //Build array of label and data handlers.
      $field_label = $value->options['ui_name'] ? $value->options['ui_name'] : $value->options['field'];
      $highcharts_label_fields[$key] = $field_label;
      if ($field_numeric) {
        $highcharts_data_fields[$key] = $field_label;
      }
    }

    $form['dataset']['label'] = array(
      '#title' => t('Data Labels Column'),
      '#type' => 'radios',
      '#options' => $highcharts_label_fields,
      '#required' => TRUE,
      '#default_value' => $this->options['dataset']['label'],
    );

    $form['dataset']['data'] = array(
      '#title' => t('Data Column(s)'),
      '#type' => 'checkboxes',
      '#options' => $highcharts_data_fields,
      '#required' => TRUE,
      '#default_value' => $this->options['dataset']['data'],
    );

  }
}