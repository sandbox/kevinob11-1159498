<?php

/**
 * @file
 * Defines the View Style Plugins for Highcharts module.
 */

/**
 * Implements hook_views_plugins().
 */
function views_format_highcharts_views_plugins() {
  return array(
    'style' => array(
      'highcharts' => array(
        'title' => t('Highcharts'),
        'help' => t('Display the results in a chart.'),
        'handler' => 'views_plugin_style_views_format_highchart',
        'uses options' => TRUE,
        'uses row plugin' => TRUE,
        'uses grouping' => FALSE,
        'type' => 'normal',
        'parent' => 'list',
        'path' => drupal_get_path('module', 'views_format_highcharts'),
        'theme' => 'highcharts',
        'theme path' => drupal_get_path('module', 'views_format_highcharts') . '/theme',
        'theme file' => 'views_format_highcharts.theme.inc',
      ),
    ),
  );
}

function views_format_highcharts_views_data_alter(&$data) {
  foreach ($data as $key => $item) {
    if (strpos($key, 'field_data') !== FALSE) {
      $data[$key][$item['entity_id']['field']['moved to']['1']]['field']['add fields to query'] = 'TRUE';
    }
  }
}
